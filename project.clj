(defproject json-app "0.1.0-SNAPSHOT"
  :description "Creates a h2 database for the country currency JSON schema"
  :url ""
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.1"] [org.clojure/data.json "1.0.0"] [com.h2database/h2 "1.3.148"] [org.clojure/java.jdbc "0.7.0"]]
  :repl-options {:init-ns json-app.core})
