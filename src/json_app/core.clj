(ns json-app.core
  (:require [clojure.data.json :as json]
    [clojure.java.jdbc :as jdb]))


;; the JSON file with all the country data  
(def file (json/read-str (slurp "resources/data.json")))

;; gets the map for each individual country given an index number for the JSON array
;; parameter x is index of json array
(def get-data-map (fn [x] ((file "country_data") x)))

;; gets the specific value for each field given the index number for the JSON array
;; and a string indicating the field name, i.e. "country_name"
;; parameter x is index of json array
;; parameter field is field name
(def get-data-value (fn [x field] ((get-data-map x) field)))

;; jdbc db configuration settings
(def json-db
  {
   :classname   "org.h2.Driver"
   :subprotocol "h2:mem"
   :subname     "jsondb;DB_CLOSE_DELAY=-1"
   :user        "sa"
   :password    ""
  }
 )

;; creating the table in the database named country_data
(jdb/db-do-commands json-db
  (jdb/create-table-ddl :country_data
    [[:country_name "varchar(50)"]
     [:country_alpha_2 "varchar(10)"]
     [:country_alpha_3 "varchar(10)"]
     [:idd_prefix "varchar(10)"]
     [:gmt_offset "varchar(12)"]
     [:time_zone_code "varchar(10)"]
     [:currency_code "varchar(10)"]
     [:currency_numeric_code "varchar(10)"]
     [:currency_name "varchar(50)"]
     [:currency_symbol "varchar(20)"]]))

;; see insert-db below
(declare insert-db)    

;; fills the database table using the helper functions above
;; calls insert db with all 10 fields, 298 times for every country in the JSON
(defn fill-country-data []
  (loop [i 0]
    (when (< i 298)
    (insert-db (get-data-value i "country_name")
    (get-data-value i "country_alpha_2")
    (get-data-value i "country_alpha_3")
    (get-data-value i "idd_prefix")
    (get-data-value i "gmt_offset")
    (get-data-value i "time_zone_code")
    (get-data-value i "currency_code")
    (get-data-value i "currency_numeric_code")
    (get-data-value i "currency_name")
    (get-data-value i "currency_symbol")) (recur (inc i)))))

;; inserts values into the database given all 10 parameters
(defn insert-db [name alpha2 alpha3 idd gmt tz currcode currnum currname currsymb]
  (jdb/execute! json-db ["insert into country_data values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" 
    name alpha2 alpha3 idd gmt tz currcode currnum currname currsymb]))  

;; wrapper function to more easily make a query 
(defn make-query [query]
  (jdb/query json-db query))
  


